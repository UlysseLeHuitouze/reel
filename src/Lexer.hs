module Lexer (Exportable, expt, Token (..), lexemes) where
import Data.Char
import Data.Foldable (foldl')
import Data.Function ((&))
import Control.Applicative





data Token = LBRACE | RBRACE | LBRACKET | RBRACKET | LCBRACKET | RCBRACKET | COMMA | SEMICOLON | BACKTICK
    | DOUBLE_DOT | INDENT (Int, Int) | MISC [Char]
    | AtomIdent String | AtomLiteralChar Char | AtomLiteral String
    | AtomBinNumber [Char] | AtomOctNumber [Char] | AtomDecNumber [Char] | AtomHexNumber [Char] 
    | IMPORT | EXPORT | AS | DATA | CLASS | INSTANCE | WHERE | PROC | FUNC | RETURN | CASE | OF | LET
    deriving (Show, Eq)


lexemes :: String -> [Token]
lexemes s = let Lexer tks = tokens in case tks s of
    Left _        -> []
    Right (_, ts) -> ts


tokens :: Lexer [Token]
tokens = many token
    where
        token = decor *> (linestart <|> word <|> number <|> special)




newtype Lexer a = Lexer (String -> Either LexerErr (String, a))
type LexerErr = String

lexerFailure :: String -> LexerErr
lexerFailure s = "Unexpected '" ++ s ++ "'"



number :: Lexer Token
number = bina <|> octa <|> hexa <|> deci
    where
        bina = string "0b" *> some (sat isDigit) & fmap AtomBinNumber
        octa = string "0o" *> some (sat isDigit) & fmap AtomOctNumber
        hexa = string "0x" *> some (sat isDigit) & fmap AtomHexNumber
        deci = (neg $ char '0' *> sat isLetter) *> some (sat isDigit) & fmap AtomDecNumber


reserved :: Lexer Token
reserved = pick [
        "(" ° LBRACE,
        ")" ° RBRACE,
        "[" ° LBRACKET,
        "]" ° RBRACKET,
        "{" ° LCBRACKET,
        "}" ° RCBRACKET,
        "," ° COMMA,
        ";" ° SEMICOLON,
        "`" ° BACKTICK,
        ".." ° DOUBLE_DOT
    ]
    where
        (°) :: String -> Token -> Lexer Token
        s ° t = string s *> pure t


special :: Lexer Token
special = reserved <|> (some nonreserved & fmap MISC)
    where
        nonreserved = (neg reserved) *> (neg $ sat isLetter) *> (neg $ sat isDigit) *>
                (neg $ sat isSpace) *> (neg $ sat isSeparator) *> sat isPrint


keyword :: Lexer Token
keyword = pick [
        "import" ° IMPORT,
        "export" ° EXPORT,
        "as" ° AS,
        "data" ° DATA,
        "class" ° CLASS,
        "instance" ° INSTANCE,
        "where" ° WHERE,
        "proc" ° PROC,
        "func" ° FUNC,
        "return" ° RETURN,
        "case" ° CASE,
        "of" ° OF,
        "let" ° LET
    ]
    where
        (°) :: String -> Token -> Lexer Token
        s ° t = string s *> pure t


word :: Lexer Token
word = Lexer $ \s ->
    let Lexer lexi = lex in case lexi s of
        Left lr        -> Left lr
        Right (cs, lr) -> let Lexer ki = keyword in case ki lr of
            Right ("", kr) -> Right (cs, kr)
            _              -> Right (cs, AtomIdent $ lr)
    where
        lex   = liftA2 (:) start (many body)
        start = char '_' <|> sat isLetter
        body  = char '_' <|> char '\'' <|> char '#' <|> sat isLetter <|> sat isDigit


linestart :: Lexer Token
linestart = char '\n' *> many (whitespace <|> tabulation) & (fmap summation)
    where
        whitespace = char ' '  *> pure (1, 0)
        tabulation = char '\t' *> pure (0, 1)
        summation  = INDENT . foldl' (+^) (0, 0)

        (a, b) +^ (c, d) = (a + c, b + d)


decor :: Lexer [String]
decor = many (space <|> commentMultiline <|> comment)
    where
        space            = some $ (neg $ char '\n') *> sat isSpace

        commentMultiline = string "###" *> body <* string "###"
            where
                body    = (many $ (neg $ string "###") *> sat true)
                true _  = True

        comment          = string "##" *> (many $ sat (/= '\n'))


















instance Functor Lexer where
    fmap f (Lexer l) = Lexer $ \s ->
        case l s of
            Left lr        -> Left lr
            Right (cs, lr) -> Right (cs, f lr)


instance Applicative Lexer where
    pure r              = Lexer $ \s -> Right (s, r)
    Lexer f <*> Lexer g = Lexer $ \s ->
        case f s of
            Left fr        -> Left fr
            Right (cs, fr) -> case g cs of
                Left gr        -> Left gr
                Right (cs, gr) -> Right (cs, fr gr)


instance Alternative Lexer where
    empty               = Lexer $ \_ -> Left "Unexpected EOF"
    Lexer f <|> Lexer g = Lexer $ \s ->
        case (f s, g s) of
            (Left _, gr) -> gr
            (fr, _)      -> fr


neg :: Lexer a -> Lexer a
neg (Lexer f) = Lexer $ \s ->
    case f s of
        Left  _ -> Right (s, undefined)
        Right _ -> Left $ lexerFailure s





pick :: [Lexer a] -> Lexer a
pick = foldl' (<|>) empty

sat :: (Char -> Bool) -> Lexer Char
sat p = Lexer $ \s ->
    case s of
        c:cs | p c -> Right (cs, c)
        []         -> Left "Unexpected EOF"
        s          -> Left $ lexerFailure s

char :: Char -> Lexer Char
char = sat . (==)

string :: String -> Lexer String
string = traverse char








class Exportable a where
    expt :: Exportable a => a -> String


instance Exportable [Token] where
    expt [] = ""
    expt ((INDENT (a, b)):ts) = (expt (INDENT (a, b))) ++ (expt ts)
    expt (t:ts) = (expt t) ++ " " ++ (expt ts)

instance Exportable Token where
    expt IMPORT = "import"
    expt EXPORT = "export"
    expt AS = "as"
    expt DATA = "data"
    expt CLASS = "class"
    expt INSTANCE = "instance"
    expt WHERE = "where"
    expt PROC = "proc"
    expt FUNC = "func"
    expt RETURN = "return"
    expt CASE = "case"
    expt OF = "of"
    expt LET = "let"
    expt (AtomIdent s) = s
    expt (AtomLiteralChar c) = '\'':c:"'"
    expt (AtomLiteral s) =  ('"':s) ++ "\""
    expt (AtomHexNumber cs) = '0':'x':cs
    expt (AtomBinNumber cs) = '0':'b':cs
    expt (AtomOctNumber cs) = '0':'o':cs
    expt (AtomDecNumber cs) = cs
    expt LBRACE = "("
    expt RBRACE = ")"
    expt LBRACKET = "["
    expt RBRACKET = "]"
    expt LCBRACKET = "{"
    expt RCBRACKET = "}"
    expt COMMA = ","
    expt SEMICOLON = ";"
    expt BACKTICK = "`"
    expt DOUBLE_DOT = ".."
    expt (INDENT (a, b)) = '\n':(take b (repeat '\t')) ++ (take a (repeat ' '))
    expt (MISC cs) = cs